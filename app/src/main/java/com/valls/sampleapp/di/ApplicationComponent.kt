package com.valls.sampleapp.di

import com.valls.sampleapp.SampleApplication
import com.valls.sampleapp.ui.main.MainModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        MainModule::class,
        AndroidSupportInjectionModule::class,
        ViewModelModule::class,
        ApplicationModule::class
    ]
)
interface ApplicationComponent : AndroidInjector<SampleApplication> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<SampleApplication>()
}