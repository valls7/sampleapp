package com.valls.sampleapp.di

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module

@Module(includes = [NetworkModule::class])
abstract class ApplicationModule {

    @Binds
    abstract fun provideApplicationContext(application: Application): Context

}