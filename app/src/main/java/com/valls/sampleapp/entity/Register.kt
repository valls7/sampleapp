package com.valls.sampleapp.entity

import java.io.Serializable

data class Register(val date: String, val age: Int): Serializable