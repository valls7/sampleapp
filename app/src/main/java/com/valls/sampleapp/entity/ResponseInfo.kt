package com.valls.sampleapp.entity

data class ResponseInfo(val seed: String, val results: Int, val page: Int, val version: String)