package com.valls.sampleapp.entity

import java.io.Serializable

data class Coordinate(val latitude: Double, val longitude: Double): Serializable