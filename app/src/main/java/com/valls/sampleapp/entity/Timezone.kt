package com.valls.sampleapp.entity

import java.io.Serializable

data class Timezone(val offset: String, val description: String): Serializable