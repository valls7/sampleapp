package com.valls.sampleapp.entity

import java.io.Serializable

data class Id(val name: String, val value: String): Serializable