package com.valls.sampleapp.entity

import java.io.Serializable

data class Birthday(val date: String, val age: Int): Serializable