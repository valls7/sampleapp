package com.valls.sampleapp.entity

import java.io.Serializable

data class User(
    val gender: String,
    val name: Name,
    val location: Location,
    val email: String,
    val login: Login,
    val dob: Birthday,
    val registered: Register,
    val phone: String,
    val cell: String,
    val id: Id,
    val picture: Picture,
    val nat: String
): Serializable