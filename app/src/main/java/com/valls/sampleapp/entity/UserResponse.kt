package com.valls.sampleapp.entity

data class UserResponse(val results: List<User>, val info: ResponseInfo)