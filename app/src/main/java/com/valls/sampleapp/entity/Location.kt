package com.valls.sampleapp.entity

import java.io.Serializable

data class Location(
    val street: String,
    val city: String,
    val state: String,
    val postcode: Int,
    val coordinates: Coordinate,
    val timeZone: Timezone
): Serializable