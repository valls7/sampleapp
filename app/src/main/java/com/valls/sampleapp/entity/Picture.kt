package com.valls.sampleapp.entity

import java.io.Serializable

data class Picture(val large: String, val medium: String, val thumbnail: String): Serializable