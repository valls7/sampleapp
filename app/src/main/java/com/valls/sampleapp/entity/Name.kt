package com.valls.sampleapp.entity

import java.io.Serializable

data class Name(val title: String, val first: String, val last: String): Serializable