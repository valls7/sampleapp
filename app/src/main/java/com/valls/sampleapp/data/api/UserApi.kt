package com.valls.sampleapp.data.api

import androidx.lifecycle.LiveData
import com.valls.sampleapp.entity.UserResponse
import retrofit2.http.GET

interface UserApi {

    @GET("?results=10&seed=xmoba")
    fun getUsers(): LiveData<ApiResponse<UserResponse>>
}