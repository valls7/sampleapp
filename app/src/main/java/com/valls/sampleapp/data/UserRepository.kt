package com.valls.sampleapp.data

import androidx.lifecycle.LiveData
import com.valls.sampleapp.data.api.ApiResponse
import com.valls.sampleapp.data.api.UserApi
import com.valls.sampleapp.entity.UserResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(private val userApi: UserApi) : Repository {

    fun getUsers(): LiveData<ApiResponse<UserResponse>> {
        return userApi.getUsers()
    }
}