package com.valls.sampleapp.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import com.valls.sampleapp.databinding.FragmentUserDetailBinding
import com.valls.sampleapp.ui.base.BaseFragment
import javax.inject.Inject

class UserDetailFragment : BaseFragment() {

    private val args: UserDetailFragmentArgs by navArgs()

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    val viewModel: UserDetailViewModel by lazy {
        val vm = ViewModelProviders.of(this, viewModelFactory).get(UserDetailViewModel::class.java)
        vm.user.value = args.user
        return@lazy vm
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentUserDetailBinding.inflate(inflater, container, false).apply {
            vm = viewModel
            lifecycleOwner = this@UserDetailFragment
        }
        return binding.root
    }
}