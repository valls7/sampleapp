package com.valls.sampleapp.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.valls.sampleapp.databinding.FragmentUserListBinding
import com.valls.sampleapp.ui.base.BaseFragment
import javax.inject.Inject

class UserListFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    val viewModel: UserListViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(UserListViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentUserListBinding.inflate(inflater, container, false)
        val adapter = UserAdapter()
        binding.userList.adapter = adapter
        subscribeUi(adapter)

        return binding.root
    }

    private fun subscribeUi(adapter: UserAdapter) {
        viewModel.users.observe(viewLifecycleOwner, Observer { users ->
            users?.body?.let { adapter.submitList(it.results) }
        })
    }

}
