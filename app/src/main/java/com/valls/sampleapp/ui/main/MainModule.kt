package com.valls.sampleapp.ui.main

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [UserListModule::class, UserDetailModule::class])
abstract class MainModule {

    @ContributesAndroidInjector
    internal abstract fun mainActivityInjector(): MainActivity
}