package com.valls.sampleapp.ui.main

import androidx.lifecycle.MutableLiveData
import com.valls.sampleapp.data.UserRepository
import com.valls.sampleapp.entity.User
import com.valls.sampleapp.ui.base.BaseViewModel
import javax.inject.Inject

class UserDetailViewModel @Inject constructor(val repository: UserRepository) : BaseViewModel() {

    val user: MutableLiveData<User> = MutableLiveData()
}