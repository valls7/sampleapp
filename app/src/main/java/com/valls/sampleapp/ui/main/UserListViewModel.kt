package com.valls.sampleapp.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.valls.sampleapp.common.AbsentLiveData
import com.valls.sampleapp.data.UserRepository
import com.valls.sampleapp.data.api.ApiResponse
import com.valls.sampleapp.entity.User
import com.valls.sampleapp.entity.UserResponse
import com.valls.sampleapp.ui.base.BaseViewModel
import javax.inject.Inject

class UserListViewModel @Inject constructor(val repository: UserRepository) : BaseViewModel() {

    val users: LiveData<ApiResponse<UserResponse>> = repository.getUsers()
}