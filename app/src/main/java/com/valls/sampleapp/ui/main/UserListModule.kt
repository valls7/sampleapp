package com.valls.sampleapp.ui.main

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class UserListModule {

    @ContributesAndroidInjector
    internal abstract fun userListFragmentInjector(): UserListFragment
}