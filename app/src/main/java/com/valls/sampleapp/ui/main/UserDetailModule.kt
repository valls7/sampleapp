package com.valls.sampleapp.ui.main

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class UserDetailModule {

    @ContributesAndroidInjector
    internal abstract fun userDetailFragmentInjector(): UserDetailFragment
}