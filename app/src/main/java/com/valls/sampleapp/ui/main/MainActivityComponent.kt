package com.valls.sampleapp.ui.main

import com.valls.sampleapp.di.ApplicationModule
import com.valls.sampleapp.ui.main.MainActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

@Subcomponent(modules = [ApplicationModule::class])
interface MainActivityComponent: AndroidInjector<MainActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<MainActivity>()
}