package com.valls.sampleapp

import com.valls.sampleapp.di.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication


class SampleApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<SampleApplication> =
        DaggerApplicationComponent.builder().create(this)
}